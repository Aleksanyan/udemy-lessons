const personalMovieDB = {
  count: 0,
  movies: {},
  actors: {},
  genres: [],
  privat: false,
  start: function() {
        personalMovieDB.count = +prompt("How many movies have you watched already?", "");

        while (personalMovieDB.count == '' || personalMovieDB.count == null || isNaN(personalMovieDB.count)) {
            numberOfFilms = +prompt("How many movies have you watched already?", "");
        }
    },
    rememberMyFilms: function() {
        for (let i = 0; i < 2; i++) {
            const lastWatchedFilm = prompt('Say One of the watched movie name?', '').trim(),
                  filmRate = +prompt('How much do you rate it?', '');
        
            if(lastWatchedFilm != null && filmRate != null && lastWatchedFilm != '' && filmRate != '' && lastWatchedFilm.length < 50) {
                personalMovieDB.movies[lastWatchedFilm] = filmRate;
                console.log('Done');
            } else {
                console.log('Error');
                i--;
            }
        
        }
    },
    detectPersonalLevel: function() {
        if (personalMovieDB.count < 10) {
            console.log("Watched quite a few films");
          } else if (personalMovieDB.count >= 10 && personalMovieDB.count < 30) {
              console.log("You are a classic viewer");
          } else if (personalMovieDB.count >= 30) {
              console.log("You are kinoman");
          } else {
              console.log("An error has occurred");
          }
    },
    showMyDB: function(hidden) {
        if (!hidden) {
            console.log(personalMovieDB);
        }
    },

    toggleVisibleMyDB: function() {
        if(personalMovieDB.privat) {
            personalMovieDB.privat = false;
        } else {
            personalMovieDB.privat = true;
        }
    },
    
    writeYourGenres: function() {
        // for(let i = 1; i <= 3; i++) {
            // let genre = prompt(`Your favorite genre number ${i}`);


            /////////////////////////
            // if (genre === '' || genre == null) {
            //     console.log("You entered incorrect data or not entered at all");
            //     i--;
            // } else {
            //     personalMovieDB.genres[i - 1] = genre;
            // } 
        // }

        for(let i = 1; i < 2; i++) {
            let genres = prompt(`Enter your favorite genre separated by commas`).toLocaleLowerCase();

            if(genres === '' || genres == null) {
                console.log("You entered incorrect data or not entered at all");
                i--;
            } else {
                personalMovieDB.genres = genres.split(', ');
                personalMovieDB.genres.sort();
            }
        }
    
        personalMovieDB.genres.forEach((item, i) => {
            console.log(`Favorite genre ${i + 1} is ${item}`);
        });
    }

};
